use poem_openapi::{Object, Enum};
use strum::Display;

#[derive(Object)]
pub struct Project {
    pub name: String,
    pub author: String,
    pub version: String,
    pub mod_loader: ModLoader,
    pub minecraft_version: String,
    pub loader_version: String,
}

#[derive(Enum, Display)]
pub enum ModLoader {
    Fabric,
    Forge,
    Quilt,
    Liteloader,
}
