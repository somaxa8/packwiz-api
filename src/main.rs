mod controller;
mod service;
mod model;

use poem::{listener::TcpListener, Route};
use poem_openapi::{OpenApiService};
use controller::packwiz::Packwiz;

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
    let controller = OpenApiService::new(Packwiz, "Packwiz Controller", "1.0").server("/api");
    let controller_ui = controller.swagger_ui();

    let app = Route::new()
                            .nest("/api", controller)
                            .nest("/", controller_ui);

    poem::Server::new(TcpListener::bind("127.0.0.1:3000"))
        .run(app)
        .await
}