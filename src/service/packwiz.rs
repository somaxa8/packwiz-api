use std::process::Command;

use crate::model::project::Project;


pub fn create_project(project: Project) {
    Command::new("packwiz")
            .current_dir("/")
            .arg("init")
            .arg(format!("--name {}", project.name))
            .arg(format!("--author {}", project.author))
            .arg(format!("--version {}", project.author))
            .arg(format!("--mc-version {}", project.minecraft_version))
            .arg(format!("--modloader {}", project.mod_loader))
            .arg(format!("--{}-version {}", project.mod_loader.to_string().to_ascii_lowercase(), project.loader_version))
            .output()
            .expect("failed to execute process");
}