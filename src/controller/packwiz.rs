use poem_openapi::{payload::{PlainText, Json}, OpenApi};

use crate::{model::project::Project, service::packwiz::create_project};

#[derive(Debug)]
pub struct Packwiz;

#[OpenApi]
impl Packwiz {

    #[oai(path = "/project", method = "post")]
    async fn post_project(&self, project: Json<Project>) -> PlainText<String> {
        create_project(project.0);
        return PlainText("tu puta madre".to_string());
    }
    
}